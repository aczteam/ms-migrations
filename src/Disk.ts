import * as fs from "fs";
import {PathLike} from "fs";
import {IDisk} from "./interfaces";

export class Disk implements IDisk {
    constructor(private dir: PathLike) {

    }

    public getDir(): PathLike {
        return this.dir;
    }

    public async getFiles(): Promise<string[]> {
        let dir = this.dir;

        return new Promise<string[]>((resolve, reject) => {
            fs.readdir(dir, (err, files) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(files);
            })
        });
    }

    public async read(file: PathLike): Promise<string> {
        let path = this.dir + "/" + file;

        return new Promise<string>((resolve, reject) => {
            fs.readFile(path, (err, data: Buffer) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(data.toString());
            });
        });
    }
}