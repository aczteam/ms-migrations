import {PoolConfig} from "mysql";
import {Migrator} from "./Migrator";
import {IMigrator} from "./interfaces";

let options: PoolConfig = {
    host: "localhost",
    database: "migratorDB2",
    user: "root",
    password: "root",
    port: 3306
};

options.host = process.env.MYSQL_HOST || options.host;

let migrator: IMigrator = new Migrator();

migrator.init(__dirname + "./../sample-migrations", options);
migrator.migrate()
    .then(() => {
        console.log("DB migrated");
    })
    .catch((err) => {
        console.log(err);
    });