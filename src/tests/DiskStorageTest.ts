import {DiskStorage} from "../DiskStorage";
import {IDisk, MigrationVO} from "../interfaces";
import {PathLike} from "fs";

describe('Test DiskStorage', () => {

    test('ttt Should read migration files and return list of migrations', async () => {

        let disk: IDisk = new TestDisk(
            ["V3__.sql", "V1__first", "V2__second", "U2__second"],
            {
                "V1__first": "1",
                "V2__second": "2",
                "U2__second": "undo2",
                "V3__.sql": "333"
            }
        );
        let diskStorage = new DiskStorage(disk);
        let migrations = await diskStorage.getMigrations();

        expect(migrations).toEqual(
            [
                {
                    version: 1,
                    up: '1',
                    down: null,
                    description: 'first',
                    hash: 'c4ca4238a0b923820dcc509a6f75849b'
                },
                {
                    version: 2,
                    up: '2',
                    down: 'undo2',
                    description: 'second',
                    hash: 'e3a8ae9bc10a15401afc7a0548ce3d22'
                },
                {
                    version: 3,
                    up: '333',
                    down: null,
                    description: '',
                    hash: '310dcbbf4cce62f762a2aaa148d556bd'
                }
            ]);

    });

    test('Should throw error on empty filename', () => {
        function parseFilename() {
            DiskStorage.parseFilename("");
        }

        expect(parseFilename).toThrowError("Error. Migration file should contain __");
    });

    test('Should throw error if prefix missing', () => {
        function parseFilename() {
            DiskStorage.parseFilename("1__");
        }

        expect(parseFilename).toThrowError("Migration file should starts with V or U char");
    });

    test('Should throw error if version missing', () => {
        function parseFilename() {
            DiskStorage.parseFilename("V__");
        }

        expect(parseFilename).toThrowError("Error. Missing version");
    });

    test('Should throw error if version not number', () => {
        function parseFilename() {
            DiskStorage.parseFilename("Vaa__");
        }

        expect(parseFilename).toThrowError("Error. Version should be number");
    });

    test('Should throw error if version not number', () => {
        let {prefix, version} = DiskStorage.parseFilename("V1__Filename.sql");
        expect(prefix).toBe("V");
        expect(version).toBe(1);
    });

    test("Should throw error if V1 file not found", async () => {
        expect.assertions(1);
        let disk: IDisk = new TestDisk(["V2__some_text.sql"]);
        let storage = new DiskStorage(disk);
        try {
            await storage.getMigrations();
        } catch (error) {
            expect(error.message).toBe("Error. Did not find V1 file");
        }
    });

    test("Should throw error if missing V* file", async () => {
        expect.assertions(1);
        let disk: IDisk = new TestDisk(
            [
                "V1__some_text.sql",
                "V2__some_text.sql",
                "V4__some_text.sql",
                "V5__some_text.sql",
                "V6__some_text.sql"
            ]
        );
        let storage = new DiskStorage(disk);
        try {
            await storage.getMigrations();
        } catch (error) {
            expect(error.message).toBe("Error. Missing V3 file");
        }
    });

    test("kkk Should throw error on duplicate file", async () => {
        expect.assertions(1);
        let disk: IDisk = new TestDisk(
            [
                "V1__some_text.sql",
                "V2__some_text.sql",
                "V3__some_text.sql",
                "V4__some_text.sql",
                "V4__some_text.sql"
            ]
        );
        let storage = new DiskStorage(disk);
        try {
            await storage.getMigrations();
        } catch (error) {
            expect(error.message).toBe("Error. Duplicate file V4");
        }
    });

});

class TestDisk implements IDisk {

    constructor(private files: string[], private filesContent?: { [key: string]: string }) {

    }

    async getFiles(): Promise<string[]> {
        return this.files;
    }

    async read(file: PathLike): Promise<string> {
        let key = file.toString();
        if (!this.filesContent || !this.filesContent[key]) {
            return "";
        }
        return this.filesContent[key];
    }

}