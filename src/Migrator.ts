import {IMigrator, MigrationVO, MigratorResult} from "./interfaces";
import {PoolConfig} from "mysql";
import {PathLike} from "fs";
import {DBStorage} from "./DBStorage";
import {DB} from "./DB";
import {DiskStorage} from "./DiskStorage";
import {Disk} from "./Disk";

export class Migrator implements IMigrator {

    private db: DB;
    private dbStorage: DBStorage;
    private diskStorage: DiskStorage;
    private options: PoolConfig;

    init(dir: PathLike, options: PoolConfig): void {
        this.options = options;
        this.db = new DB(options);
        this.dbStorage = new DBStorage(this.db);
        let disk = new Disk(dir);
        this.diskStorage = new DiskStorage(disk);
    }

    public async migrate(): Promise<MigratorResult> {
        await this.db.createDatabaseIfNeeded();
        await this.db.createMigrationsTableIfNeeded();

        let migrations: MigrationVO[] = await this.diskStorage.getMigrations();
        for (let m of migrations) {
            await this.executeMigration(m);
        }
        return {};
    }

    public async dropTestDatabase(): Promise<MigratorResult> {

        if (this.options.database && this.options.database.toLowerCase().indexOf("test") != -1) {
            await this.db.dropDatabase();
        } else {
            throw new Error("Can't drop NOT test database!");
        }

        return {};
    }


    private async executeMigration(m: MigrationVO) {
        if (await this.dbStorage.shouldExecute(m)) {
            console.log(`Executing migration V:${m.version} description:${m.description}`);
            try {
                await this.db.query(m.up as string, {});
            } catch (err) {
                console.error(err);
                m.state = 'failed';
                m.errorStack = err.stack;
            }
            //am not really sure if we should save migration if it fails but meanwhile I'd save the error stack
            await this.dbStorage.addMigration(m);
        } else {
            try {
                let hash = await this.dbStorage.getMigrationHash(m.version);
                if (hash !== m.hash) {
                    throw new Error(`Migration file ${m.version}-${m.description} seems to be corrupted!`);
                }
            } catch (error) {
                console.error(error);
            }
        }
    }

}