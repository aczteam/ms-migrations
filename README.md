# ms-migrator
Migrate MySQL database using pure sql migration files.
Migration files naming same as in [flywebdb](https://flywaydb.org/documentation/migrations#naming-1)

## Features:
1. Uses `PoolConfig` interface from mysql module
2. Will automatically create database if not exists
3. Will create `migrations` table in same database to store
migrations metadata
4. Has **ONLY ONE COMMAND** `migrate`
Other commands which allow navigate over migrations like `revert`
 **CONCEPTUALLY NOT SUPPORTED**
5. Undo migration file is optional and used **ONLY** to revert last unsuccessful
migration.
6. For example, if you need revert migration `V4__Add_index_to_column_id.sql`
You have to create new migration `V5__Remove_index_from_column_id.sql`
7. After execution database schema always will be in determined
version state. If migrating to version `N` fails migrator will undo `N` migration
so schema will be at `N-1` version.

## Usage
1. Create migrations dir
2. Create migration files (see name rules)
3. Config migrator using `PoolConfig` from mysql module
4. Run migrator

## File name rules:
1. All migrations should be in same dir
2. All migrations should have `.sql` file extension
3. Migration file should contain valid SQL statement
Multi statements supported
5. Up migration file should starts with `V`(V=version) down with `U` (U=Undo)
7. After prefix should be number of version started from 1, incrementing with step 1
8. Double low dash (`__`) separate migration name
Sample name: `V1_Create_table_users.sql`