import {PoolConfig} from "mysql";
import {PathLike} from "fs";

export interface IMigrator {

    init(migrationsDir: string, poolConfig: PoolConfig): void

    /*
     *   async function
     *   throws MigratorError
     */
    migrate(): Promise<MigratorResult>;
}

export interface IDisk {
    getFiles(): Promise<string[]>

    read(file: PathLike): Promise<string>
}

export interface MigratorError {

}

export interface MigratorResult {

}

/*
    VO = Value Object
 */
export interface MigrationVO {
    version: number;
    description?: string | undefined | null;
    state?: string
    errorStack?: string
    up: string | undefined | null;
    down: string | undefined | null;
    hash?: string
}

export interface IDiskStorage {
    /*
        throws DiskStorageError
     */
    getMigrations(migrationsDir: string): Promise<MigrationVO[]>
}

export interface DiskStorageError {

}