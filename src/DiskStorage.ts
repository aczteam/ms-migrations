import {IDisk, IDiskStorage, MigrationVO} from "./interfaces";

export class DiskStorage implements IDiskStorage {

    constructor(private disk: IDisk) {

    }

    public async getMigrations(): Promise<MigrationVO[]> {
        let files = await this.disk.getFiles();

        let check = DiskStorage.checkFilesNames(files);
        if (check !== true) {
            throw new Error(check as string);
        }

        let map: Map<number, MigrationVO> = new Map();

        for (let file of files) {
            let {prefix, version, description} = DiskStorage.parseFilename(file);

            if (!map.has(version)) {
                let migration: MigrationVO = {
                    version: version,
                    up: null,
                    down: null,
                    description: null
                };
                map.set(version, migration);
            }

            // "as MigrationVO" is hack because
            // tsc thinking that "m.get(version)" can be undefined
            let currentMigration = map.get(version) as MigrationVO;
            let data = await this.disk.read(file);
            currentMigration.hash = DiskStorage.getCheckSum(data);
            if (prefix == "V") {
                currentMigration.up = data;
                currentMigration.description = description;
            } else {
                (map.get(version) as MigrationVO).down = data;
                currentMigration.down = data;
            }
        }


        let arr: MigrationVO[] = [];
        for (let i = 1; i <= map.size; i++) {
            if ((map.get(i) as MigrationVO).up) {
                arr.push(map.get(i)as MigrationVO);
            }
        }

        return arr;
    }

    private static getCheckSum(data) {
        let crypt = require("crypto");
        return crypt
            .createHash('md5')
            .update(data, 'utf8')
            .digest('hex');
    }

    private static checkFilesNames(files: string[]): string | boolean {

        let up: Map<number, string> = new Map();
        let down: Map<number, string> = new Map();

        for (let file of files) {
            let {prefix, version} = DiskStorage.parseFilename(file);

            if (prefix == "V") {
                if (up.has(version)) {
                    return `Error. Duplicate file V${version}`;
                }
                up.set(version, "");
            } else {
                if (down.has(version)) {
                    return `Error. Duplicate file V${version}`;
                }
                down.set(version, "");
            }
        }

        if (!up.has(1)) {
            return "Error. Did not find V1 file";
        }

        for (let i = 1; i <= up.size; i++) {
            if (!up.has(i)) {
                return (`Error. Missing V${i} file`);
            }
        }

        return true;
    }

    static parseFilename(file) {
        let arr = file.split("__");
        if (!arr[0]) {
            throw new Error("Error. Migration file should contain __");
        }
        let s = arr[0];
        let description = arr[1].split(".")[0];
        let prefix = s.charAt(0);
        if (prefix != "V" && prefix != "U") {
            throw new Error("Error. Migration file should starts with V or U char");
        }

        let versionString = s.replace(prefix, "");
        if (versionString.length == 0) {
            throw new Error("Error. Missing version");
        }

        if (isNaN(versionString)) {
            throw new Error("Error. Version should be number");
        }

        let version: number = parseInt(versionString);

        return {prefix, version, description};
    }
}