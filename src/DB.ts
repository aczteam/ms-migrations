import * as mysql from "mysql";
import {Connection, MysqlError, PoolConfig} from "mysql";

export class DB {
    constructor(private options: PoolConfig) {

    }

    public async dropDatabase(): Promise<void> {

        // Need options without database property
        let o = Object.assign({}, this.options);
        delete o.database;

        let conn: Connection = mysql.createConnection(o);
        try {
            let sql = "DROP DATABASE IF EXISTS ??";
            let result = await this.connectionQuery(conn, sql, [this.options.database]);
            if (result.warningCount == 0) {
                console.log(`Database ${this.options.database} dropped`);
            }
        } catch (dbError) {
            let mysqlError: MysqlError = dbError as MysqlError;

            console.log(mysqlError.code);
            console.log(mysqlError.sql);
            console.log(mysqlError.message);
            throw dbError;
        } finally {
            await conn.end();
        }
    }

    public async createDatabaseIfNeeded(): Promise<void> {

        // Need options without database property
        let o = Object.assign({}, this.options);
        delete o.database;

        let conn: Connection = mysql.createConnection(o);
        try {
            let sql = "CREATE DATABASE IF NOT EXISTS ??";
            let result = await this.connectionQuery(conn, sql, [this.options.database]);
            if (result.warningCount == 0) {
                console.log(`Database ${this.options.database} created`);
            }
        } catch (dbError) {
            let mysqlError: MysqlError = dbError as MysqlError;

            console.log(mysqlError.code);
            console.log(mysqlError.sql);
            console.log(mysqlError.message);
            throw dbError;
        } finally {
            await conn.end();
        }
    }

    public async createMigrationsTableIfNeeded() {
        let sql =
            `
            CREATE TABLE IF NOT EXISTS migrations (
                version INT(11) NOT NULL,
                description VARCHAR(255) NOT NULL,
                state VARCHAR(50) NOT NULL,
                error_stack TEXT,
                createdAt DATETIME NOT NULL,
                hasUndo VARCHAR(50) NOT NULL,
                hash TEXT,
                PRIMARY KEY (version)
            )
            ENGINE=InnoDB;
            `;

        await this.query(sql, {});
    }

    public async query(sql: string, options?): Promise<any> {
        this.options.multipleStatements = true;
        let conn: Connection = mysql.createConnection(this.options);

        let result = await this.connectionQuery(conn, sql, options);
        await conn.end();
        return result;
    }

    private async connectionQuery(conn: Connection, sql: string, options?): Promise<any> {
        return new Promise<any>(((resolve, reject) => {
            conn.query(sql, options, (err: MysqlError | null, results?: any) => {
                if (err) {
                    reject(err);
                    return;
                }

                resolve(results);
            });
        }));
    }
}