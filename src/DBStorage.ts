import {DB} from "./DB";
import {MigrationVO} from "./interfaces";

export class DBStorage {
    constructor(private db: DB) {

    }

    public async shouldExecute(migration: MigrationVO): Promise<boolean> {
        let sql = `SELECT * FROM migrations WHERE version=? AND state='success'`;
        let result = await this.db.query(sql, [migration.version]);
        if (result[0]) {
            return false;
        }

        sql = `SELECT * FROM migrations WHERE version=? AND state='failed'`;
        result = await this.db.query(sql, [migration.version]);

        if (result[0]) {
            return true;
        }

        return true;
    }

    public async getMigrationHash(version): Promise<string> {
        let sql = `SELECT * FROM migrations WHERE version=? AND state='success'`;
        let result = await this.db.query(sql, [version]);
        let migration = Object.assign({}, result[0]);
        return migration["hash"];
    }

    public async addMigration(m: MigrationVO): Promise<void> {
        let sql = ` 
                INSERT INTO migrations 
                (version, description, state,error_stack,hash, createdAt, hasUndo) 
                VALUES (?, ?, ?, ?, ?, NOW(), 'no');
                `;
        let description = m.description || "";
        let error_stack = m.errorStack || "";
        let state = m.state || "success";
        await this.db.query(sql, [m.version, description, state, error_stack, m.hash]);
    }
}